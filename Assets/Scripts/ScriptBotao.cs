﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptBotao : MonoBehaviour
{

    private int life = 200;
    private int contador = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.down * (Time.deltaTime * 4); 
        contador++;
        if(contador>=life){
            ScriptCamera.quantNotas+=1;
            ScriptCamera.quantErros+=1;
            Destroy(gameObject);
            contador = 0;
        }
    }
}
