﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScriptGeneros : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void selecionarPOP(){
        ScriptMusicData.genero = "POP";
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    public void selecionarPOPROCK(){
        ScriptMusicData.genero = "POP ROCK";
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    public void selecionarROCK(){
        ScriptMusicData.genero = "ROCK";
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    public void selecionarHEAVYROCK(){
        ScriptMusicData.genero = "HEAVY ROCK";
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    public void selecionarREGGAE(){
        ScriptMusicData.genero = "REGGAE";
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
