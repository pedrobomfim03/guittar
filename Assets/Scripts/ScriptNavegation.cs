﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScriptNavegation : MonoBehaviour
{

    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void clickMenuPrincipal(){
		SceneManager.LoadScene(0, LoadSceneMode.Single);
	}

	public void clickGenero(){
		SceneManager.LoadScene(1, LoadSceneMode.Single);
	}

    public void clickMusica(){
		SceneManager.LoadScene(2, LoadSceneMode.Single);
	}

    public void clickDificuldade(){
		SceneManager.LoadScene(3, LoadSceneMode.Single);
	}

    public void clickGame(){
		SceneManager.LoadScene(4, LoadSceneMode.Single);
	}

    public void DificuldadeFacil(){
        ScriptMusicData.dificuldade = 1;
		clickGame();
	}

    public void DificuldadeNormal(){
        ScriptMusicData.dificuldade = 2;
		clickGame();
	}

    public void DificuldadeDificil(){
        ScriptMusicData.dificuldade = 3;
		clickGame();
	}

    public void DificuldadeExtremo(){
        ScriptMusicData.dificuldade = 4;
		clickGame();
	}

    public void clickSair(){
		Application.Quit();
	}
}
